<?php

use App\Http\Controllers\EmployeeController;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [EmployeeController::class, 'loadAll']);
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Route::get('/home', [EmployeeController::class, 'loadAll']);

Route::get('/add', [EmployeeController::class, 'add']);

Route::get('/import', [EmployeeController::class, 'import']);

Route::get('/add-employee', [EmployeeController::class, 'addEmployee']);

Route::post('/get-employee', [EmployeeController::class, 'getEmployee']);

Route::get('/delete-employee/{employee_code}', [EmployeeController::class, 'deleteEmloyee']);

Route::get('/edit-employee/{employee_code}', [EmployeeController::class, 'editEmloyee']);

Route::post('/update-employee', [EmployeeController::class, 'updateEmloyee'])->name('update-employee');

Route::post('/import-csv', [EmployeeController::class, 'import_csv']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
