<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Thêm Nhân Viên</title>
</head>

<body>
    <div style="margin-top: 5%;margin-left: 10%;">
        <h1 style="color: #007acc;">Thêm Nhân Viên</h1>
        <a href="/home" style="text-decoration: none;">Danh Sách Nhân Viên |</a>
        <a href="/add" style="text-decoration: none;">Thêm Nhân Viên |</a>
        <a href="/import" style="text-decoration: none;">Import từ CSV</a>
    </div>

    <div style="width: 50%;margin-top: 1%;margin-left: 10%;">
        <form action="get-employee" method="POST">
            @csrf
            <!-- Employee Code
            <input class="form-control form-control-sm" type="text" aria-label=".form-control-sm example" name="code"> -->

            <div class="input-group mb-3 p-xl-1">
                <span class="input-group-text" id="inputGroup-sizing-default">Employee Code</span>
                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="code">
            </div>

            <div class="input-group mb-3 p-xl-1 ">
                <span class="input-group-text" id="inputGroup-sizing-default">Nhập Tên</span>
                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="name">
            </div>

            <div class="input-group mb-3 p-xl-1">
                <span class="input-group-text" id="inputGroup-sizing-default">Email</span>
                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="email">
            </div>

            <div class="input-group mb-3 p-xl-1">
                <span class="input-group-text" id="inputGroup-sizing-default">Số điện thoại</span>
                <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="phone">
            </div>

            <div class="input-group mb-3 p-xl-1">
                <span class="input-group-text" id="inputGroup-sizing-default">Thời gian làm việc</span>
                <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="time">
            </div>

            <input type="submit" class="btn btn-primary btn-lg" value="Thêm Nhân Viên"></input>
        </form>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
</body>

</html>