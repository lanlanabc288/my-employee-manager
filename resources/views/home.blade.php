<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Ngô Híu</title>
</head>

<body>

    @if(session('alert'))
        <section class='alert alert-success'>{{session('alert')}}</section>
    @endif  

    @extends('layouts.app')

    @section('content')
    <div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <!-- <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        {{ __('You are logged in!') }}
                    </div>
                </div> -->

                <div style="margin-top: 5%;margin-left: 10%;">
                    <h1 style="color: #007acc;">Quản Lý Nhân Viên</h1>
                    <a href="/home" style="text-decoration: none;">Danh Sách Nhân Viên |</a>
                    <a href="/add" style="text-decoration: none;">Thêm Nhân Viên |</a>
                    <a href="/import" style="text-decoration: none;">Import từ CSV</a>
                </div>

                <table class="table table-primary" style="width: 80%;margin-top: 2%;margin-left: 10%;">
                    <thead>
                        <tr>
                            <th scope="col">Số thứ tự</th>
                            <th scope="col">Mã Nhân Viên</th>
                            <th scope="col">Họ và Tên</th>
                            <th scope="col">Email</th>
                            <th scope="col">Số Điện Thoại</th>
                            <th scope="col">Thời gian làm việc</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt = 1 ?>
                        @foreach ($info as $value)
                        <tr>
                            <th scope="row"><?= $stt++ ?></th>
                            <td>{{ $value->employee_code}}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->email }}</td>
                            <td>{{ $value->phone }}</td>
                            <td>{{ $value->time }}</td>
                            <td>
                                <a href="edit-employee/{{ $value->employee_code }}" class="btn btn-link">Sửa</a>
                            </td>
                            <td>
                                <a href="delete-employee/{{ $value->employee_code }}" class="btn btn-link">Xóa</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection



    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
</body>

</html>