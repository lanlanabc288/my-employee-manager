<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Import Nhân Viên Từ CSV</title>

</head>

<body>
    <div style="margin-top: 5%;margin-left: 10%;">
        <h1 style="color: #007acc;">Import Nhân Viên Từ CSV</h1>
        <a href="/home" style="text-decoration: none;">Danh Sách Nhân Viên |</a>
        <a href="/add" style="text-decoration: none;">Thêm Nhân Viên |</a>
        <a href="/import" style="text-decoration: none;">Import từ CSV</a>
    </div>

    <div style="width: 80%;margin-top: 2%;margin-left: 10%;">
        <form action="{{url('import-csv')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" accept=".csv"><br>
            <br>
            <input type="submit" value="Import CSV" name="import_csv" class="btn btn-warning">
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
</body>

</html>