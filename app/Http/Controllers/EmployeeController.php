<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\EmployeeImport;
use Excel;

class EmployeeController extends Controller
{
    public function addEmployee()
    {
        $employee = [
            ["employee_code" => "HieuNQhx", "name" => "Ngô Quang Hiếu", "email" => "lanlanabc288@gmail.com", "phone" => "0975079848", "time" => "25"],
            ["employee_code" => "QuangBC14", "name" => "Trần Đức Quang", "email" => "quang@gmail.com", "phone" => "0123456789", "time" => "25"],
            ["employee_code" => "RedRed98", "name" => "Trần Hải Yến", "email" => "redred288@gmail.com", "phone" => "0999999999", "time" => "25"]
        ];

        Employee::insert($employee);
        return "Đã thêm vào";
    }

    public function loadAll()
    {
        $employee['info'] = DB::table('employees')->get()->toArray();
        return view('/home', $employee);
    }

    public function import()
    {
        return view('/import');
    }

    public function add()
    {
        return view('/add');
    }

    public function getEmployee(Request $request)
    {
        $allEmployee = DB::table('employees')->get()->toArray();
        $code = $request->code;

        $query = "SELECT * FROM `employees` WHERE employee_code = ? ";
        $db = DB::select($query, [$code]);

        if (!empty($db)) {
            $alert = 'Mã nhân viên đã tồn tại , hãy nhập 1 mã nhân viên khác';
            return redirect('/home')->with('alert', $alert);
        }

        DB::table('employees')->insert([
            'employee_code' => $request->code,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'time' => $request->time
        ]);

        $alert = 'Đã thêm nhân viên vào danh sách';
        return redirect('/home')->with('alert', $alert);
    }

    public function deleteEmloyee(Request $request)
    {
        // echo $request->employee_code;
        DB::table('employees')->where('employee_code', $request->employee_code)->delete();
        $alert = 'Đã xóa nhân viên được chọn';
        return redirect('/home')->with('alert', $alert);
    }

    public function editEmloyee(Request $request)
    {
        // echo $request->employee_code;
        $result['info'] = DB::table('employees')->where('employee_code', $request->employee_code)->get()->toArray();
        // print_r($result);
        return view('/edit', $result);
    }

    public function updateEmloyee(Request $request)
    {
        echo $request->name;

        $result['info'] = DB::table('employees')->where('employee_code', $request->employee_code)->update(['name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'time' => $request->time]);

        $alert = 'Đã sửa nhân viên được chọn';
        return redirect('/home')->with('alert', $alert);
    }

    public function import_csv(Request $request)
    {
        $path = $request->file('file')->getRealPath();
        Excel::import(new EmployeeImport, $path);

        $alert = 'Đã thêm nhân viên từ file csv';
        return redirect('/home')->with('alert', $alert);
    }
}
