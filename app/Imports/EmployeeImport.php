<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\ToModel;

class EmployeeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Employee([
            'employee_code' => $row[0],
            'name' => $row[1],
            'email' => $row[2],
            'phone' => $row[3],
            'time' => $row[4],
        ]);
    }
}
